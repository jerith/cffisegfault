import os.path

from cffi import FFI
ffi = FFI()
ffi.cdef("""
typedef ... arr;

arr* new_arr(size_t);
void trash_arr(arr*);
size_t arr_size(arr*);
bool put_item(arr*, long);
long get_item(arr*);
""")

mydir = os.path.split(__file__)[0]

with open(os.path.join(mydir, 'stack.c')) as cfile:
    lib = ffi.verify(cfile.read(), libraries=[])


class CStack(object):
    def __init__(self, capacity):
        self._arr = lib.new_arr(capacity)

    def __del__(self):
        self.trash()

    def trash(self):
        if self._arr is None:
            return
        lib.trash_arr(self._arr)
        self._arr = None

    def __len__(self):
        return lib.arr_size(self._arr)

    def put(self, item):
        lib.put_item(self._arr, item)

    def get(self):
        return lib.get_item(self._arr)
