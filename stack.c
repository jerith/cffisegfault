#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

typedef bool (*less_t)(void*, void*);

typedef struct {
    size_t capacity;
    size_t size;
    long *items;
} arr;


arr *new_arr(size_t capacity) {
    long *items = malloc(capacity * sizeof(long));
    arr *arr = malloc(sizeof(arr));
    arr->capacity = capacity;
    arr->size = 0;
    arr->items = items;
    return arr;
}


void trash_arr(arr *arr) {
    free(arr->items);
    free(arr);
}


size_t arr_size(arr *arr) {
    return arr->size;
}


void _set(arr *arr, size_t i, long item) {
    arr->items[i - 1] = item;
}


long _get(arr *arr, size_t i) {
    return arr->items[i - 1];
}


bool put_item(arr *arr, long item) {
    /* printf("put: %lu %l\n", arr->size, item); */
    if (arr->size >= arr->capacity) return false;
    _set(arr, ++arr->size, item);
    return true;
}


long get_item(arr *arr) {
    /* printf("get: %lu\n", arr->size); */
    return _get(arr, arr->size--);
}
