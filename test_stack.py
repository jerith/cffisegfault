import unittest
import random
from stack import CStack


class Stacker(object):
    def __init__(self):
        self.items = []
        self.stack = CStack(100)

    def check_put(self, item):
        assert len(self.stack) == len(self.items)
        if len(self.items) > 90:
            return
        self.stack.put(item)
        self.items.append(item)

    def check_get(self):
        assert len(self.stack) == len(self.items)
        if len(self.items) < 1:
            return
        # item = min(self.items)
        # self.items.remove(item)
        item = self.items.pop()
        assert self.stack.get() == item

    def apply(self, op_data):
        if op_data[0] == 'put':
            self.check_put(op_data[1])
        elif op_data[0] == 'get':
            self.check_get()
        else:
            assert False


def mk_ops():
    ops = []
    for _ in range(random.randint(0, 100)):
        ops.append(
            (random.choice(['get', 'put']), random.randint(-1000, 1000)))
    return ops


class TestThing(unittest.TestCase):
    def test_ops(self):
        for i in range(50):
            print "run:", i + 1
            ops = mk_ops()
            stacker = Stacker()
            for op in ops:
                stacker.apply(op)
            stacker.stack.trash()


if __name__ == '__main__':
    unittest.main()
